<?php

$parent     = $this->session->userdata("parent");
$activeItem = $this->session->userdata("activeItem");

?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url("assets");?>/dist/img/user1-160x160.jpg" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Mustafa bozkaya</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
                <a href="<?php echo base_url("admin"); ?>"
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            </li>

            <li class="treeview <?php echo ($parent == "kullanıcı_folder") ? "active" : ""; ?>">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Kullanıcı İşlemleri</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu" id="kullanıcı_folder">
                    <li class="<?php echo ($activeItem == "") ? "active" : ""; ?>" id="">
                        <a href="<?php echo base_url("allusersimg");?>"><i class="fa fa-circle-o"></i> Fotograflar</a>
                    </li>
                    <li class="<?php echo ($activeItem == "") ? "active" : ""; ?>" id="">
                        <a href="<?php echo base_url("userlist");?>"><i class="fa fa-circle-o"></i> Kullanıcılar</a>
                    </li>
                </ul>

                <a href="#">
                    <i class="fa fa-building"></i>
                    <span>FI & Co Modülü</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <a href="#">
                    <i class="fa fa-balance-scale"></i>
                    <span>Marketing & Sales modülü</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <a href="#">
                    <i class="fa fa-truck"></i>
                    <span>Production modülü</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <a href="#">
                    <i class="fa fa-chain"></i>
                    <span>SCM Modülü</span>
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>


            </li>



        </ul>

    </section>
    <!-- /.sidebar -->
</aside>