
<html>
<head>
    <?php $this->load->view("panel/includes/head"); ?>
    <?php $this->load->view("panel/users/list/page_style"); ?>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php $this->load->view("panel/includes/header"); ?>
    <!-- Left side column. contains the logo and sidebar -->

    <?php $this->load->view("panel/includes/left_side_bar"); ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Page Header
                <small>Optional description</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
                <li class="active">Here</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if(isset($isadmin)) { ?>
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center alert alert-success alert-dismissible show" role="alert">
                        <strong>Hoşgeldiniz  <?php echo $admin->kullanici_ad; ?>!!</strong> :)
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>

            <?php } ?>
            <!-- Your Page Content Here -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    <?php $this->load->view("panel/includes/footer"); ?>

<!-- REQUIRED JS SCRIPTS -->

<?php $this->load->view("panel/users/list/page_script"); ?>
</body>
</html>
