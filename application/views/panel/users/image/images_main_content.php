
<!-- Main content -->
<section class="content">

    <?php if (!isset($admin)) { ?>
    <div class="row">
        <div class="col-md-12">
            <form action="<?php echo base_url("users/uploadimage");?>" class="dropzone" id="mydrop">
            </form>
        </div>
    </div>

    <?php }?>

    <br>
    <div class="row">
        <div class="col-md-3">
            <a  href="<?php if (isset($single)) {
                $user_id=$this->session->userdata('user_id'); echo  base_url("admin/imageUploadPage/$user_id");
            }else {
                echo  base_url("allusersimg");
            }?>"><strong><i class="fa fa-refresh"></i>Refresh Page </strong></a>
        </div>
        <div class="col-md-6 text-center">
            <strong><i class="fa fa-file-image-o"></i> <?php echo "Toplam". $num_rows." Kayıt Getirldi" ;?></strong>
        </div>
        <div class="col-md-3 " style="float: right">
            <a href="<?php if (!isset($admin)) {
                echo  base_url("users/ayarlar_page");
            }else {
                echo  base_url("userlist");
            }
            ?>" style="float: right" ><strong><i class="glyphicon glyphicon-arrow-right"></i>Back User Page</strong></a>
        </div>


    </div>
    <br>
    <div class="row">

        <div class="col-md-12">

            <div class="box">
                <div class="box-body table-responsive no-padding ">
                    <table class="table table-hover ">
                        <thead>
                            <th>Önizleme</th>
                            <th>image Name</th>
                            <th>User ID</th>
                            <th>Active</th>
                            <th>Cover</th>
                            <th class="col-md-2">İşlemler</th>
                        </thead>
                        <tbody class="sortableList" postUrl="<?php if (!isset($admin)) {
                            echo  "ayarlar/userImageRankUpdate";
                        }else {
                            echo  "admin/userImageRankUpdate";
                        }
                        ?>">
                        <?php foreach($rows as $row) { ?>
                            <tr id="sortId-<?php echo $row->img_id;?>">
                                <td>
                                    <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url("uploads/") .$row->img_name; ?>">
                                        <img style="height: 80px"
                                            src="<?php echo base_url("uploads/"). $row->img_name; ?>"
                                            alt="<?php echo $row->img_name; ?>"
                                            class="img-responsive"
                                        />
                                    </a>

                                </td>
                                <td><?php echo $row->img_name;?></td>
                                <td><?php echo $row->user_id;?></td>
                                <td>
                                    <input class = "toggle_check"
                                           id="isactive"
                                           data-onstyle="success"
                                           data-size = "mini"
                                           data-on="Aktif"
                                           data-off="Pasif"
                                           data-offstyle="danger"
                                           type="checkbox"
                                           data-toggle="toggle"
                                           dataID="<?php echo $row->img_id; ?>"
                                        <?php echo ($row->is_active == 1) ? "checked" : ""; ?>
                                    />
                                </td>
                                <td>
                                    <input class = "toggle_check"
                                           id="iscover"
                                           data-onstyle="success"
                                           data-size = "mini"
                                           data-on="Aktif"
                                           data-off="Pasif"
                                           data-offstyle="danger"
                                           type="checkbox"
                                           data-toggle="toggle"
                                           dataID="<?php echo $row->img_id; ?>"
                                        <?php echo ($row->is_cover == 1) ? "checked" : ""; ?>
                                    />
                                </td>
                                <td>
                                    <a class="removeBtn" href="<?php
                                        echo base_url("admin/deleteImage/$row->img_id");
                                     ?>">
                                        <i class="fa fa-trash" style="font-size:16px;">Delete</i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

</section>
<!-- /.content -->