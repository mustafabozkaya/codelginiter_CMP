<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
<!--            <a href="--><?php //echo  base_url("ayarlar/newPage");?><!--" class="btn btn-sm btn-primary mb10"><i class="fa fa-plus"></i> Ekle</a>-->
            <div class="box">
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <th>İD</th>
                            <th>Kullanıcı Name</th>
<!--                            <th>Boyut(m<sup>2</sup>)</th>-->
                            <th>E-Posta</th>
                            <th>password</th>
                            <th>tarih</th>
                            <th>is_administer</th>
                            <th class="col-md-2">İşlemler</th>
                        </thead>
                        <tbody class="sortableList" postUrl="ayarlar/rankUpdate">
                            <?php foreach($rows as $row) { ?>
                                <tr id="sortId-<?php echo $row->id;?>">
                                    <td><?php echo $row->id;?></td>
                                    <td><?php echo $row->kullanici_ad;?></td>
                                    <td><?php echo $row->email;?></td>
                                    <td><?php echo $row->password;?></td>
                                    <td><?php echo $row->tarih;?></td>
                                    <td>
                                        <input class = "toggle_check"
                                               data-onstyle="success"
                                               data-size = "mini"
                                               data-on="Aktif"
                                               data-off="Pasif"
                                               data-offstyle="danger"
                                               type="checkbox"
                                               data-toggle="toggle"
                                               dataID="<?php echo $row->id; ?>"
                                            <?php echo ($row->is_administer == 1) ? "checked" : ""; ?>
                                        >
                                    </td>
                                    <td>
                                        <?php if (!isset($admin)) {?>
                                        <a href="<?php echo base_url("ayarlar/editPage/$row->id"); ?>">

                                            <i class="fa fa-edit" style="font-size:16px;">Edit</i>
                                        </a> |
                                        <?php }?>

                                        <?php if (isset($admin)) {?>

                                        <a class="removeBtn"  href="<?php echo base_url("admin/delete/$row->id");?>">
                                            <i class="fa fa-trash-o" style="font-size:16px;">Delete</i>
                                        </a> |
                                        <?php }?>

                                        <a href="<?php
                                            echo base_url("admin/imageUploadPage/$row->id");
                                        ?>">
                                            <i class="fa fa-image" style="font-size:16px;">Edit İmages</i>
                                        </a>


                                    </td>
                                </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->