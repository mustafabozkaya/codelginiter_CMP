<section class="content-header">
    <h1>
        Kullanıcılar
        <small>Kullanıcı listesi</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
        <li class="active">Kullanıcı İşlemleri</li>
        <li class="active">Fotoğraf Ekle</li>
    </ol>
</section>