<script src="<?php echo base_url("assets/"); ?>dist/js/third_party/bootstrap-toggle.min.js"></script>

<script>

    $(document).ready(function () {



        // Bootstrap Toggle init
        $('.toggle_check').bootstrapToggle();

        // is_active Change

        $('.toggle_check').change(function () {

            var isAdminister = $(this).prop('checked');
            var base_url = "<?php echo isset( $_SESSION["baseUrl"]) ?  $_SESSION["baseUrl"] : "+$(''.base_url').text()+" ;?>";
            var id       = $(this).attr("dataID");
            $.post(base_url + "admin/isAdministerSetter", {id: id, isAdminister: isAdminister}, function (response) {});

        })


    })

</script>