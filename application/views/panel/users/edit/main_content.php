<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <!-- form start -->
                <form role="form" method="post" action="<?php echo base_url("ayarlar/edit/$row->id");?>">
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Kullanıcı Adı</label>
                            <input type="text" class="form-control" name="title" placeholder="Kullanıcı adını giriniz.." value="<?php echo $row->kullanici_ad; ?>">                        </div>
                    </div>
                    <div class="box-body col-md-6">
                        <div class="form-group">
                            <label for="exampleInputKullanıcı">Kullanıcı Adı</label>
                            <input type="text" class="form-control" name="kullanici_ad" placeholder="Kullanıcı adını giriniz.." value="<?php echo $row->kullanici_ad; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">E-Posta</label>
                            <input type="text" class="form-control" name="email" placeholder="Emailinizi  giriniz.." value="<?php echo $row->email; ?>">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputsifre">Şifre</label>
                            <input type="text" class="form-control" name="password" placeholder=" yeni parola  giriniz.." value="<?php echo $row->password; ?>">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="clearfix"></div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Kaydet</button>
                    </div>
                </form>
            </div>
            <!-- /.box -->
        </div>
</section>
<!-- /.content -->