<!doctype html>
<html lang="tr">
<head>
    <?php $this->load->view("panel/includes/head"); ?>
    <?php $this->load->view("panel/users/image/page_style"); ?>
</head>
<body class="fixed skin-blue sidebar-mini sidebar-collapse">

<div class="container">

    <?php

        $this->load->view("panel/includes/header");
     ?>

    <!-- Left side column. contains the logo and sidebar -->
    <?php

        $this->load->view("panel/includes/left_side_bar");
    ?>

    <div class="">
        <?php $this->load->view("panel/users/image/breadcrumb"); ?>
        <!-- Content Wrapper. Contains page content -->
        <?php $this->load->view("panel/users/image/images_main_content"); ?>
    <!-- /.content-wrapper -->
    </div>



</div>

<?php $this->load->view("panel/includes/footer"); ?>
<?php $this->load->view("panel/users/image/page_script"); ?>

</body>
</html>