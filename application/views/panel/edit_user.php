<!doctype html>
<html lang="tr">
<head>
    <?php $this->load->view("panel/includes/head"); ?>
    <?php $this->load->view("panel/users/edit/page_style"); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    <?php $this->load->view("panel/includes/header"); ?>
    <!-- Left side column. contains the logo and sidebar -->

    <?php $this->load->view("panel/includes/left_side_bar"); ?>


    <div class="content-wrapper">
        <?php $this->load->view("panel/users/edit/breadcrumb"); ?>
        <!-- Content Wrapper. Contains page content -->
        <?php $this->load->view("panel/users/edit/main_content"); ?>
    <!-- /.content-wrapper -->
    </div>


</div>

<?php $this->load->view("panel/includes/footer"); ?>
<?php $this->load->view("panel/users/edit/page_script"); ?>

</body>
</html>