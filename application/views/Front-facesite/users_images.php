
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ansayfa</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/dist/css/third_party/dropzone.css"/>
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/dist/css/third_party/bootstrap-toggle.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url("assets");?>/dist/css/third_party/jquery.fancybox.min.css" media="screen">


    <style>

        .dropzone{
            border: 2px dashed #d4d4d4;
            height: auto;
            background-color: #fff;
            color: black;
            font-size: 20px;
            font-style: italic;
        }

        .gallery
        {
            display: inline-block;
            margin-top: 20px;
        }

        .thumbnail {
            width: 90px;
            height:90px;
        }
    </style>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid"><span class=" baseurl hidden"> <?php echo base_url();?> </span>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="#">
                <img style="padding-bottom:10px ;width: 18px ;height: 30px" alt="Brand"  src="<?php if (!isset($active_userimg)) {
                    echo base_url('uploads/')."defaultimg/avatar1.png";

                }else{
                    echo base_url('uploads/').$active_userimg->img_name;

                } ?>" alt="">
            </a>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo base_url('ayarlar/').$active_user->id; ?>">Ayarlar <span class="sr-only">(current)</span></a></li>


            </ul>
            <ul class="nav navbar-nav">
                <li class=""><a href="<?php echo base_url('anasayfa/'.sha1($active_user->email)) ?>">Anasayfa <span class="sr-only">(current)</span></a></li>


            </ul>
            <form action="<?php  echo base_url('cikis/'.sha1($active_user->email)); ?>" class="navbar-form navbar-right">
                <button type="submit" class="btn btn-primary">Çıkış</button>

            </form>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<h3 class="text-capitalize text-center text-success"> Fotograflar</h3>
<hr/><p><?php echo "active_userimg :";  print_r($active_userimg);?></p>
<hr/><p><?php echo "sessions :";  print_r($_SESSION);?></p>
<hr/><p><?php echo "last ınsert user :";  print_r($this->user_model->get_lastinsert());?></p>
<hr/><p><?php echo "last ınsert user :";  print_r($this->userimage_model->get_lastinsert());?></p>
<!-- Main content -->
<div class="container">
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo base_url("users/uploadimage");?>" class="dropzone" id="mydrop">
                </form>
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-md-3">
                <a  href="<?php $user_id=$this->session->userdata('user_id'); echo  base_url("users/imageUploadPage/$user_id");?>"><strong><i class="fa fa-refresh"></i>Refresh Page</strong></a>
            </div>
            <div class="col-md-3 " style="float: right">
                <a href="<?php echo  base_url("users/ayarlar_page/$active_user->id");?>" style="float: right" ><strong><i class="glyphicon glyphicon-arrow-right"></i>Back User Page</strong></a>
            </div>


        </div>
        <br>
        <div class="row">

            <div class="col-md-12">

                <div class="box">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                            <th>İmage ID</th>
                            <th>Önizleme</th>
                            <th>image Name</th>
                            <th>User ID</th>
                            <th>Active</th>
                            <th>Cover</th>
                            <th class="col-md-2">İşlemler</th>
                            </thead>
                            <tbody class="sortableList" postUrl="ayarlar/userImageRankUpdate">
                            <?php foreach($rows as $row) { ?>
                                <tr id="sortId-<?php echo $row->img_id;?>">

                                    <td><?php echo $row->img_id;?></td>
                                    <td>
                                        <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo base_url("uploads/") .$row->img_name; ?>">
                                            <img
                                                style="height: 80px"
                                                src="<?php echo base_url("uploads/"). $row->img_name; ?>"
                                                alt="<?php echo $row->img_name; ?>"
                                                class="img-responsive"
                                            />
                                        </a>

                                    </td>
                                    <td><?php echo $row->img_name;?></td>
                                    <td><?php echo $row->user_id;?></td>
                                    <td>
                                        <input class = "toggle_check"
                                               id="isactive"
                                               data-onstyle="success"
                                               data-size = "mini"
                                               data-on="Aktif"
                                               data-off="Pasif"
                                               data-offstyle="danger"
                                               type="checkbox"
                                               data-toggle="toggle"
                                               dataID="<?php echo $row->img_id; ?>"
                                            <?php echo ($row->is_active == 1) ? "checked" : ""; ?>
                                        />
                                    </td>
                                    <td>
                                        <input class = "toggle_check"
                                               id="iscover"
                                               data-onstyle="success"
                                               data-size = "mini"
                                               data-on="Aktif"
                                               data-off="Pasif"
                                               data-offstyle="danger"
                                               type="checkbox"
                                               data-toggle="toggle"
                                               dataID="<?php echo $row->img_id; ?>"
                                            <?php echo ($row->is_cover == 1) ? "checked" : ""; ?>
                                        />
                                    </td>
                                    <td>
                                        <a class="removeBtn" href="<?php echo base_url("users/deleteImage/$row->img_id"); ?>">
                                            <i class="fa fa-trash" style="font-size:16px;">Delete</i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>

    </section>
</div>

<!-- /.content -->

<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js")?>"></script>
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js")?>"></script>
<script src="<?php echo base_url("assets"); ?>/dist/js/third_party/dropzone.js"></script>
<script src="<?php echo base_url("assets"); ?>/dist/js/third_party/bootstrap-toggle.min.js"></script>
<script src="<?php echo base_url("assets"); ?>/dist/js/third_party/jquery.fancybox.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url("assets/");?>plugins/slimScroll/jquery.slimscroll.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        // Dropzone.autoDiscover = false;// bunu yazmazsak dropzon otomatik olarak clası dropzone olan form elemtinden dropzonu çalıştırıyor
        // ikinci defa aşagıdaki gibi çalıştırdıgımızda
        // Uncaught Error: Dropzone already attached. hatası verıyor


        $(".dropzone").dropzone();

        // Bootstrap Toggle init
        $('.toggle_check').bootstrapToggle();

        // is_active or is_cover  Change

        $('.toggle_check').change(function () {

            var active = $(this).prop('checked');
            var base_url = "<?php echo isset( $_SESSION["baseUrl"]) ?  $_SESSION["baseUrl"] : "+$(''.base_url').text()+" ;?>";
            var id       = $(this).attr("dataID");
            var is_togglebtn  = $(this).attr("id");

            /*alert(active);
             alert(base_url);
             alert(id);
             alert(is_togglebtn);
             alert (typeof($(this).attr("id")));
             window.alert(is_togglebtn =="iscover");
             window.alert(is_togglebtn ==="iscover");*/



            if ($(this).attr("id") === "iscover") {
                $.post(base_url + "users/isCoverSetterForImage", {id: id, is_cover: active}, function (response) {});

            }else{
                $.post(base_url + "users/isActiveSetterForImage", {id: id, is_active: active}, function (response) {});

            }




        })


    })

    $(document).ready(function(){
        //FANCYBOX
        //https://github.com/fancyapps/fancyBox
        $(".fancybox").fancybox({
            openEffect: "none",
            closeEffect: "none"
        });
    });






</script>

</body>
</html>




