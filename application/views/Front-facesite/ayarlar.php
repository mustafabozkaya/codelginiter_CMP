
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ansayfa</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid"><span class=" baseurl hidden"> <?php echo base_url();?> </span>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img style="padding-bottom:10px ;width: 18px ;height: 30px" alt="Brand"  src="<?php if (!isset($active_userimg)) {
                    echo base_url('uploads/')."defaultimg/avatar1.png";

                }else{
                    echo base_url('uploads/').$active_userimg->img_name;

                } ?>" alt="">
            </a>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo base_url('ayarlar/').$active_user->id; ?>">Ayarlar <span class="sr-only">(current)</span></a></li>


            </ul>
            <ul class="nav navbar-nav">
                <li class=""><a href="<?php echo base_url('anasayfa/'.sha1($active_user->email)) ?>">Anasayfa <span class="sr-only">(current)</span></a></li>


            </ul>
            <form action="<?php  echo base_url('cikis/'.sha1($active_user->email)); ?>" class="navbar-form navbar-right">
                <button type="submit" class="btn btn-primary">Çıkış</button>

            </form>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<h3 class="text-capitalize text-center text-success"> Düzenleme Sayfası</h3><hr>
<!--//<p>--><?php //echo "sessions :";  print_r($_SESSION);?><!--</p>-->
<div class="container">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                            <th>İD</th>
                            <th>Kullanıcı Name</th>
                            <th>E-Posta</th>
                            <th>password</th>
                            <th>Kayıt Tarih</th>
                            <th class="col-md-2">İşlemler</th>
                            </thead>
                            <tbody class="sortableList" postUrl="ayarlar/rankUpdate">

                            <tr id="sortId-<?php echo $active_user->id;?>">
                                <td><?php echo $active_user->id;?></td>
                                <td><?php echo $active_user->kullanici_ad;?></td>
                                <td><?php echo $active_user->email;?></td>
                                <td><?php echo $active_user->password;?></td>
                                <td><?php echo $active_user->tarih;?></td>

                                <td>
                                    <a href="<?php echo base_url("users/editPage/$active_user->id");?>">
                                        <i class="fa fa-edit" style="font-size:16px;">Edit</i>
                                    </a> |
                                    <!--<a class="removeBtn"  href="<?php /*echo base_url("users/delete/$active_user->id"); */?>">
                                        <i class="fa fa-trash-o" style="font-size:16px;">Delete</i>
                                    </a> |-->

                                    <a href="<?php echo base_url("users/imageUploadPage/$active_user->id"); ?>">
                                        <i class="fa fa-image" style="font-size:16px;">Edit İmages</i>
                                    </a>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>



<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js")?>"></script>
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js")?>"></script>
</body>
</html>




