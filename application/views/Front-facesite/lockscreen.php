<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Lockscreen</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap'); ?>/css/bootstrap.min.css">
  <!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>dist/css/third_party/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/dist'); ?>/css/AdminMB.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition lockscreen">
<!--//<p>--><?php //echo "sessions :";  print_r($_SESSION);?><!--</p>-->
<?php if($logout) { ?>
  <div class="row">
    <div class="col-md-6 col-md-offset-3 text-center alert alert-warning alert-dismissible show" role="alert">
      <strong>Good BY <?php echo $logout_user->kullanici_ad; ?>!!</strong> yine bekleriz:)
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  </div>
<?php } ?>
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="<?php echo base_url('logout'); ?>"><b>Admin Lockscreen</b></a>
  </div>
  <!-- User name -->
  <div class="lockscreen-name"><small style="text-transform: capitalize; color: #00a7d0">
    <?php if($logout) {
      $user=$logout_user->kullanici_ad;

      echo $user;
    }else{
      $user=$lastlogout_user->kullanici_ad;

      echo "\t Do you $user?";
    }

    ?>
    </small>
  </div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="
      <?php if (!$logout) {
        echo base_url('uploads/')."defaultimg/avatar1.png";

      }else{
        echo base_url('uploads/').$user_profilimg->img_name;

      }
      ?>" alt="User Image">

    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->
    <form class="lockscreen-credentials" action="<?php echo base_url('girisyap2'); ?>" method="post" >

      <div class="input-group">
        <input type="hidden" name="eposta" class="form-control" value="<?php echo ($logout) ? $logout_user->email : $lastlogout_user->email;?>" >
        <input type="password" name="sifre" class="form-control" placeholder="password">

        <div class="input-group-btn">
          <button type="submit" class="btn  btn-instagram"><i class="fa fa-arrow-right text-muted"> Giriş</i></button>
        </div>
      </div>
    </form>
    <!-- /.lockscreen credentials -->

  </div>
  <!-- /.lockscreen-item -->
  <div class="help-block text-center">
    Enter your password to retrieve your session
  </div>
  <div class="text-center">
    <a href="<?php echo base_url('giris'); ?>">Or sign in as a different user</a>
  </div>
  <div class="lockscreen-footer text-center">
    Copyright &copy; <?php echo date("Y"); ?>
    <b><a href="<?php echo base_url('logout'); ?>" class="text-black">Mustafa Bozkaya</a></b><br>
    All rights reserved
  </div>
</div>
<!-- /.center -->

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url('assets'); ?>/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url('assets/bootstrap'); ?>/js/bootstrap.min.js"></script>
</body>
</html>
