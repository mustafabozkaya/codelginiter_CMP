
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ansayfa</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid"><span class=" baseurl hidden"> <?php echo base_url();?> </span>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img style="padding-bottom:10px ;width: 18px ;height: 30px" alt="Brand"  src="<?php if (!isset($active_userimg)) {
                    echo base_url('uploads/')."defaultimg/avatar1.png";

                }else{
                    echo base_url('uploads/').$active_userimg->img_name;

                } ?>" alt="">
            </a>

        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">

                <li class="active"><a href="<?php echo base_url('ayarlar/'.$active_user->id); ?>">Ayarlar <span class="sr-only">(current)</span></a></li>


            </ul>
            <ul class="nav navbar-nav">
                <li class=""><a href="<?php echo base_url('anasayfa/'.sha1($active_user->email)) ?>">Anasayfa <span class="sr-only">(current)</span></a></li>


            </ul>
            <form action="<?php  echo base_url('cikis/'.sha1($active_user->email)); ?>" class="navbar-form navbar-right">
                <button type="submit" class="btn btn-primary">Çıkış</button>

            </form>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<h3 class="text-capitalize text-center text-success"> Anasayfa</h3>
<!--//<p>--><?php //echo "sessions :";  print_r($_SESSION);?><!--</p>-->
    <div class="container">
        <div class="col-lg-10 well col-lg-offset-1">
            <div class="row">
                <div class="col-md-12">
                    <div class="thumbnail col-lg-12">
                        <img src="<?php if (!isset($active_userimg)) {
                            echo base_url('uploads/')."defaultimg/avatar1.png";

                        }else{
                            echo base_url('uploads/').$active_userimg->img_name;

                        } ?>" alt="...">
                        <div class="caption">
                            <h3 style="color: #00CC00">Hoşgeldiniz <?php if (isset($login)){ echo $active_user->kullanici_ad;}?></h3>

                        </div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Error, nostrum, tempora! Alias dignissimos, distinctio earum fuga, impedit itaque modi natus non porro praesentium quam quas quis quo rem sapiente voluptatum?
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid aspernatur blanditiis cupiditate dignissimos doloribus dolorum ipsam iusto, magnam, minima nesciunt provident ut. Aut deleniti, dicta minus obcaecati officia quos rem.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js")?>"></script>
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js")?>"></script>
</body>
</html>




