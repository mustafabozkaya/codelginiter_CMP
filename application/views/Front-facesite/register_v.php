<!doctype html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <title>Register Forms</title>

    <style>
        body{
            background-color: #525252;
        }
        .form-group{
            margin-top: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        input.btn{
            margin-top: 10px;
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .centered-form{
            margin-top: 60px;
        }

        .centered-form .panel{
            background-color: #f5f5f5;
            box-shadow: rgba(0, 0, 0.3, 0.3) 20px 20px 20px;
        }
    </style>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>dist/css/third_party/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" id="bootstrap-css">

    <!------ Include the above in your HEAD tag ---------->
</head>
<body>
    <?php if(isset($new_user)) { ?>
    <div class="row ">
        <div class="col-md-6 col-md-offset-3 text-center alert alert-success alert-dismissible show" role="alert">
            <strong>Bilgileriniz Başarrılı şekilde Kayıt edildi</strong>
            <a href="<?php echo base_url('giris'); ?>">Giriş yapamak için Tıklayın</a>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
    <?php } ?>
    <h3 class="text-center"> Kayıt Formu</h3><br>
    <div class="container">
        <div class="row">
            <div class="col-md-6 well col-lg-offset-3">
                <form action="<?php echo base_url('register');?>" method="post">
                    <div class="form-group">
                        <label>Kullanıcı Adınız</label>
                        <input type="text" name="kullanici_ad" class="form-control" placeholder="Kullanıcı Adınız">
                        <?php if(isset($form_error)) { ?>
                            <small  class="pull-right" style="color: red"><?php echo form_error("kullanici_ad"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">
                        <label>E-posta Adresiniz</label>
                        <input type="email" name="email" class="form-control" placeholder="E-posta">
                        <?php if(isset($form_error)) { ?>
                            <small  class="pull-right" style="color: red"><?php echo form_error("email"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group centered-form"
                          style="
                          border: 1px solid #cecece;
                          padding: 10px;
                          border-radius: 5px;
                          background-color: white; height: 40px ;width:inherit;">
                        <label class="col-xs-3" for="cinsiyet">Cinsiyet  </label>
                        <div class="col-xs-2">
                            <div class="input-group">
                                <label class=" radio-inline">
                                    <input type="radio" name="cinsiyet" id="inlineRadio1" value="1" checked> Erkek
                                </label>

                            </div><!-- /input-group -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-xs-2">
                            <div class="input-group">

                                <label class="radio-inline">
                                    <input type="radio" name="cinsiyet" id="inlineRadio2" value="0"> Kadın
                                </label>

                            </div><!-- /input-group -->
                        </div><!-- /.col-lg-6 -->
                    </div><!-- /.row -->
                    <div class="form-group">
                        <label>Şifre</label>
                        <input type="password" name="password" class="form-control" placeholder="Şifre">
                        <?php if(isset($form_error)) { ?>
                            <small class="pull-right" style="color: red"><?php echo form_error("sifre"); ?></small>
                        <?php } ?>
                    </div>

                    <div class="form-group">

                        <label for="password_confirm">Şifre Tekrar</label>
                        <input type="password" id="password_confirm" name="password_confirm" class="form-control " placeholder="Şifre">
                        <?php if(isset($form_error)) { ?>
                            <small class="pull-right" style="color: red"><?php echo form_error("password_confirm"); ?></small>
                        <?php } ?>
                        <hr>
                        <button type="submit" class="btn  btn-primary btn-block">Gönder</button><br>
                    </div>
                        <a  class="col-md-6  col-md-offset-3" style="text-align: center" href="<?php echo base_url('giris'); ?>">
                                <i class="fa fa-arrow-right centered-form">Geri dön</i>
                        </a>
                </form>

            </div>
        </div>
    </div>

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</body>
</html>

