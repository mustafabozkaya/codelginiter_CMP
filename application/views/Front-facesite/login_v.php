
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kullanıcı Giriş-Çıkış</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
</head>
<body>
<?php if(isset($user_error)) { ?>
    <div class="row ">
        <div class="col-md-6 col-md-offset-3 text-center alert alert-danger alert-dismissible show" role="alert">
            <strong>Hey !!</strong> Ne yapıyorsun  kayıtlı degilsin.Aramıza katılamk için kayıt ol..
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>

<?php } ?>
<?php if(isset($logout)) { ?>
    <div class="row">
        <div class="col-md-6 text-center alert alert-warning alert-dismissible show" role="alert">
            <strong>Good BY <?php echo $logout_user->kullanici_ad; ?>!!</strong> yine bekleriz:)
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>

<?php } ?>

    <h3 class="text-center"><?php echo  isset($admin) ? "Admin Giriş" :"Giriş" ?> </h3><br>
    <div class="container">
        <div class="row">
            <div class="col-md-6 well col-lg-offset-3">
                <form action="<?php echo (isset($admin)) ? base_url('admin/admingiris') : base_url('girisyap'); ?>" method="post">
                    <div class="form-group">
                        <label>E-posta Adresiniz</label>
                        <input type="email" name="eposta" class="form-control" placeholder="E-posta">
                        <?php if(isset($form_error)) { ?>
                            <small  class="pull-right" style="color: red"><?php echo form_error("eposta"); ?></small>
                        <?php } ?>
                    </div>
                    <div class="form-group">
                        <label>Şifre</label>
                        <input type="password" name="sifre" class="form-control" placeholder="Şifre">
                        <?php if(isset($form_error)) { ?>
                            <small class="pull-right" style="color: red"><?php echo form_error("sifre"); ?></small>
                        <?php } ?>
                        <hr>
                    <button type="submit" class="btn  btn-primary btn-block">Giriş</button><br>
                        <?php if (!isset($admin)) { ?>
                            <a  type="button"   class="btn  btn-primary btn-block" href="<?php echo base_url('registerpage');?>">Yeni Kayıt</a>
                        <?php }?>

                </form>

            </div>
        </div>
    </div>

<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js")?>"></script>
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js")?>"></script>

</body>
</html>




