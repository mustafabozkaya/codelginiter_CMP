
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ansayfa</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">


</head>

<body>
<nav class="navbar navbar-default ">
    <div class="container-fluid"><span class=" baseurl hidden"> <?php echo base_url();?> </span>
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img style="vertical-align: middle; width: 20px" alt="Brand"  src="<?php
                    echo base_url('uploads/')."admin/logo.png";
                ?>" alt="MB PANEL"></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Oturum İşlemleri <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url('giris');?>">Log in User</a></li>
                        <li><a href="<?php echo base_url('admin/adminlogin');?>">Log in Administer</a></li>

                    </ul>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center text-uppercase">
            <h5>Sağ köşeden Oturum işlemlerinden Giriş yapnız</h5>
            <hr/>
        </div>
    </div>
    <div class="row ">
            <div class="col-md-6 col-md-offset-3 text-center text-uppercase ">
                <h5>Web sayfam için beyne tıklayınız....</h5>
                <a href="http://blog.mustafabozkaya.tk"  target="_blank" class="thumbnail">
                    <img src="<?php echo base_url('uploads/admin/brain_logo.png'); ?>"
                          alt="http://blog.mustafabozkaya.tk"
                         title="Mustafa BOZKAYA Oficial Site">
                </a>
            </div>
        </div>

</div>


<script src="<?php echo base_url("assets/plugins/jQuery/jquery-2.2.3.min.js")?>"></script>
<script src="<?php echo base_url("assets/bootstrap/js/bootstrap.min.js")?>"></script>
</body>
</html>





