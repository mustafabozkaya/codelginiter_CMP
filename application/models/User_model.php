<?php
class User_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }
    public function get($where = array()){
        return $this->db->where($where)->get("kullanicilar")->first_row();// koşulu saglayan varsa geriye o kaydı döndürür yoksa null döndürür --boş döner yani---
    }

    public function get_orderby($order = array(),$options="asc"){
        return $this->db->order_by($order[0],$options)->get("kullanicilar")->first_row();// koşulu saglayan varsa geriye o kaydı döndürür yoksa null döndürür --boş döner yani---
    }

    public function get_lastinsert()
    {
        return $this->db->insert_id();
    }

    public function getall(){
        return  $this->db->query("SELECT * FROM kullanicilar ;")->result();// koşulu saglayan varsa geriye o kaydı döndürür yoksa null döndürür --boş döner yani---
    }

    public function update($where = array(),$update=array()){

        /*$updata = array(
            'title' => $title,
            'name' => $name,
            'date' => $date
        );*/

        return $this->db->where($where)->update('kullanicilar', $update);
    }
    public function insert_user($insert=array()){

        $insertdata = array(
            'kullanici_ad' => $insert["kullanici_ad"] ,
            'email' => $insert["email"] ,
            'cinsiyet' => $insert["cinsiyet"],
            'password' => $insert["password"]
        );

        return $this->db->insert('kullanicilar', $insertdata);
    }



    public function delete($where = array(),$array_table=array("kullanicilar")){

       // $array_table = array('table1', 'table2', 'table3');

        $this->db->where($where)->delete($array_table);
    }
}