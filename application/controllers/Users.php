<?php
class Users extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
        //echo "mustafa bozkaya";
        $this->load->view("Front-facesite/index");

    }

    public function home()
    {  $id=$sha1_user_email=1;

        $users_list= $this->session->userdata("loginuser_liste");

        print_r($_SESSION);

        die();
        $active_user=$users_list[$id];

        $active_userimg = $this->userimage_model->get(array(

            "user_id" => $active_user->id

        ));



        $v_data=new stdClass();
        $v_data->active_user = $active_user;// v_data objemize user indeksiyle active_user objesini atıyoruz
        $v_data->active_userimg = $active_userimg;//
        $v_data->login=true;//
        $this->load->view("Front-facesite/homepage_v",$v_data);
    }


    public function home_page($sha1_user_email)
    {  $id=$sha1_user_email;

        $users_list= $this->session->userdata("loginuser_liste");
        $active_user=$users_list[$id];



        $active_userimg = $this->userimage_model->get(array(

            "user_id" => $active_user->id,
            "is_active" =>1,
            "is_cover" =>1

        ));



        $v_data=new stdClass();
        $v_data->active_user = $active_user;// v_data objemize user indeksiyle active_user objesini atıyoruz
        $v_data->active_userimg = $active_userimg;//
        $v_data->login=true;//
        $this->load->view("Front-facesite/homepage_v",$v_data);
    }


    public function ayarlar_page($userid){

        $this->session->set_userdata("baseUrl", base_url());


        $active_user=$this->user_model->get(array(
            "id" => $userid
        ));

        $active_userimg = $this->userimage_model->get(array(
            "user_id" => $userid,
            "is_active" =>1,
            "is_cover" =>1
        ));



        $v_data=new stdClass();
        $v_data->active_user=$active_user;// v_data objemize user indeksiyle active_user objesini atıyoruz
        $v_data->active_userimg=$active_userimg;//
        $v_data->login=true;//
    $this->load->view("Front-facesite/ayarlar",$v_data);
    }

    public function editPage($userid){

        $active_user=$this->user_model->get(array(
            "id" => $userid
        ));

        $active_userimg = $this->userimage_model->get(array(
            "user_id" => $userid,
            "is_active" =>1,
            "is_cover" =>1
        ));

        $v_data=new stdClass();
        $v_data->active_user=$active_user;// v_data objemize user indeksiyle active_user objesini atıyoruz
        $v_data->active_userimg=$active_userimg;//
        $v_data->login=true;//

        $this->load->view("Front-facesite/edit_user", $v_data);


    }

    public function edit($id){

        $data = array(
            "kullanici_ad" => $this->input->post("kullanici_ad"),
            "email" => $this->input->post("email"),
            "password" => $this->input->post("password")
        );

        $update = $this->user_model->update(
            array("id"	=> $id),
            $data
        );

        if($update){

            $updateuser = $this->user_model->get(
                array("id"	=> $id)
            );

            $users_list= $this->session->userdata("loginuser_liste");
            $users_list[sha1($updateuser->email)]=$updateuser;
            $this->session->set_userdata("loginuser_liste",$users_list);

            redirect(base_url("ayarlar/$id"));
        }else{
            redirect(base_url("Front-facesite/editPage/$id"));
        }
    }


    public function delete($id)
    {
        $this->user_model->delete(array('id'=>$id));

        $users_list= $this->session->userdata("loginuser_liste");

        foreach ($users_list as $deleteuser) {
            if ($deleteuser->id==$id) {

                unset($users_list[sha1($deleteuser->email)]);
                $this->session->set_userdata("loginuser_liste",$users_list);

            }
        }

    }




    public function login_form()
    {
        $this->load->view("Front-facesite/login_v");
    }

    public function register_form()
    {

        $this->load->view("Front-facesite/register_v");
    }

    public function register()
    {


        $form_kural = array(
            array(
                'field' => 'kullanici_ad',
                'label' => 'Kullanıcı Adı',
                'rules' => 'trim|required|min_length[5]|max_length[20]|is_unique[kullanicilar.kullanici_ad]'
            ),
            array(
                'field' => 'password',
                'label' => 'Parola',
                'rules' => 'trim|required|min_length[5]|max_length[20]|alpha_numeric'
            ),
            array(
                'field' => 'password_confirm',
                'label' => 'İkinci parola bilgisi',
                'rules' => 'trim|required|min_length[5]|max_length[20]|matches[password]|alpha_numeric'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|valid_email|is_unique[kullanicilar.email]'
            )
        );

        if (!class_exists('CI_Form_validation'))
            $this->load->library('form_validation');


        $this->form_validation->set_rules($form_kural);
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_message('required', "<b>{field}</b> alanını boş bırakamazsınız");
        $this->form_validation->set_message('matches', 'Parolalar eşleşmiyor.');
        $this->form_validation->set_message('valid_email', 'Geçerli email adresi giriniz.');
        $this->form_validation->set_message(array(
            "min_length" => "<b>{field}</b> alanını en az 5 karakter uzunlugunda olmalı",
            "max_length" => "<b>{field}</b> alanını en fazla 15 karakter uzunlugunda olmalı"
        ));

        if ($this->form_validation->run() === TRUE) {


            $kullanici_adi = $this->input->post('kullanici_ad');
            $parola = $this->input->post('password');
            $email = $this->input->post('email');
            $cinsiyet = $this->input->post('cinsiyet');

            if ($this->user_model->insert_user([

                "kullanici_ad" => $kullanici_adi,
                "password" => $parola,
                "email" => $email,
                "cinsiyet" => $cinsiyet])
            ) {


                $lastad_user = $this->user_model->get_orderby(["tarih"], "desc");


                $last_userimg = $this->userimage_model->add([

                    "user_id" => $lastad_user->id,
                    "img_name" => "defaultimg/avatar1.png",
                    "is_active" => "1"
                ]);


                $viewData = new stdClass();
                $viewData->new_user = true;
                $this->load->view("Front-facesite/register_v", $viewData);
            }
        } else {
            //echo validation_errors('<div class="alert alert-warning" role="alert">', '</div>');

            echo "<br>form_validation dogru çalışmadı";
            $viewData = new stdClass();
            $viewData->form_error = true;
            $this->load->view("Front-facesite/register_v", $viewData);
        }

    }


    public function login()
    {
        $this->form_validation->set_rules("eposta", "E-Posta", "required|trim|valid_email");
        $this->form_validation->set_rules("sifre", "Şifre", "trim|required|min_length[5]|max_length[15]|alpha_numeric");


        $this->form_validation->set_message(array(
            "required"      => "<b>{field}</b> alanını boş bırakamazsınız",
            "valid_email"   => "Lütfen geçerli bir <b>E-Posta</b> adresi giriniz",
            "min_length"   => "<b>{field}</b> alanını en az 5 karakter uzunlugunda olmalı",
            "max_length"   => "<b>{field}</b> alanını en fazla 15 karakter uzunlugunda olmalı"
        ));
        if($this->form_validation->run() == FALSE){

            //echo validation_errors('<div class="alert alert-warning" role="alert">', '</div>');


            $viewData = new stdClass();
            $viewData->form_error = true;
            $this->load->view("Front-facesite/login_v", $viewData);

        }
        else {
            $user = $this->user_model->get(
                array(
                    "email" => $this->input->post("eposta"),
                    "password"  => $this->input->post("sifre")
                )
            );




            if($user){



                if($this->session->userdata("loginuser_liste")){ // loginuser_liste adında bir index -key var varsa 1 yoksa null döner.ilkinde olmadıgından buraya giremiycek


                    $user_list = $this->session->userdata("loginuser_liste"); //$user_list adlı arrayimiz burdan çıkınca sıfırlandıgı için eski kulanıcıları sessions dan alıyoruz
                }
                else {


                    $user_list = [];
                }

                $user_list[sha1($user->email)] = $user;// sessions dan yukarıda aldımız kullanıcılara ek olarak yeni kullanıcıyı da ekliyoruz


                $this->session->set_userdata("loginuser_liste", $user_list);// şimdi de user_list adındaki tüm  kullanıcı listesini session'a loginuser_liste indexsiyle tekrar atıyoruz


                redirect(base_url("anasayfa/" . sha1($user->email)));
            }
            else {

                $viewData = new stdClass();
                $viewData->user_error = true;
                $this->load->view("Front-facesite/login_v", $viewData);
            }
        }

    }
    public function login2()
    {

            $user = $this->user_model->get(
                array(
                    "email" => $this->input->post("eposta"),
                    "password"  => $this->input->post("sifre")
                )
            );


            if($user){


                if($this->session->userdata("loginuser_liste")){ // loginuser_liste adında bir index -key var varsa 1 yoksa null döner.ilkinde olmadıgından buraya giremiycek


                    $user_list = $this->session->userdata("loginuser_liste"); //$user_list adlı arrayimiz burdan çıkınca sıfırlandıgı için eski kulanıcıları sessions dan alıyoruz
                }
                else {

                    $user_list = [];// sayfa ilk kez yüklendiğinde session da loginuser_liste index daha atanmadıgından bu kısım çalışacak

                }

                $user_list[sha1($user->email)] = $user;// sessions dan yukarıda aldımız kullanıcılara ek olarak yeni kullanıcıyı da ekliyoruz

                //echo "geçerli giriş yapıldıgında  \$user_list içerigi:" ;print_r($user_list);echo "<hr>";

                $this->session->set_userdata("loginuser_liste", $user_list);// şimdi de user_list adındaki tüm  kullanıcı listesini session'a loginuser_liste indexsiyle tekrar atıyoruz



                redirect(base_url("anasayfa/" . sha1($user->email)));
            }
            else {

                $viewData = new stdClass();
                $viewData->user_error = true;
                $this->load->view("Front-facesite/login_v", $viewData);
            }


    }


    public function logout($sha1_user_email)
    {


//        echo $id;
//        $this->kullanıcıları_listele();

        $users_list= $this->session->userdata("loginuser_liste");



        if (array_key_exists ($sha1_user_email,$users_list)) {


            $logoutuser_list = [];

            $logout_user=$users_list[$sha1_user_email];//çıkış yapan kullanıcı

            $logoutuser_list[$sha1_user_email]=$logout_user;
//        echo "<hr>";
//        print_r($logout_user);
            unset($users_list[$sha1_user_email]);// $id anahtarına denk gelen   kullanıcıyı $id si ile siliyor

            $this->session->set_userdata("loginuser_liste",$users_list);// kalan kullanıcıları tekrar sessionsa atıyoruz
//        $this->kullanıcıları_listele();

            $_SESSION["logoutuser_list"]=$logoutuser_list;//çıkş yapan kullanıcılarrı sessiona ekliyoruz


            $user_profilimage=$this->userimage_model->get([
                "user_id" =>$logout_user->id,
                "is_active" =>1,
                "is_cover" =>1
            ]);


            $v_data=new stdClass();
            $v_data->logout_user=$logout_user;// v_data objemize user indeksiyle logout_user objesini atıyoruz
            $v_data->logout=true;
            $v_data->user_profilimg=$user_profilimage;
            $this->load->view("Front-facesite/lockscreen",$v_data);

        }
        else {
            /*echo "logout";
            die();*/
            $logoutuser_list=$_SESSION["logoutuser_list"];
            $lastlogout_user=$logoutuser_list[$sha1_user_email];
            $v_data=new stdClass();
            $v_data->lastlogout_user=$lastlogout_user;// v_data objemize  logout_user objesini atıyoruz
            $v_data->logout=false;
            $this->load->view("Front-facesite/lockscreen",$v_data);
        }


    }

    public function imageUploadPage($kullanici_id){

        $this->session->set_userdata("user_id", $kullanici_id);


        $_vData = new stdClass();

        $_vData->rows = $this->userimage_model->get_all(
            array(
                "user_id"	=> $kullanici_id,
            ),
            "img_id  ASC"
        );

        $active_user=$this->user_model->get(array(
            "id" => $kullanici_id
        ));

        $active_userimg = $this->userimage_model->get(array(
            "user_id" => $kullanici_id,
            "is_active" =>1,
            "is_cover" =>1
        ));


        $_vData->active_user=$active_user;// v_data objemize user indeksiyle active_user objesini atıyoruz
        $_vData->active_userimg=$active_userimg;//
        $_vData->login=true;//

        $this->load->view("Front-facesite/users_images", $_vData);

    }

    public function uploadimage(){



        $config['upload_path']          = 'uploads/';
        //$config['allowed_types']        = '*';
        $config['allowed_types']        = 'jpg|gif|png|mp4';
        $config['encrypt_name']			= true;
        $config['max_size']             = 100;

        /* $config['max_width']            = 1024;
         $config['max_height']           = 768;*/

        $this->load->library('upload', $config);

//		if(!file_exists(FCPATH)){}

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());



            print_r($error);

        }
        else
        {

            // Upload Basarili ise DB ye aktar..
            $data = array('upload_data' => $this->upload->data());
            /*print_r($data);
            echo "<hr>";*/
            $img_name = $data["upload_data"]['file_name'];
//            print_r($img_name);
//            echo "<hr>";
//            print_r($data);
////           die();
            $user_id=$this->session->userdata("user_id");

            $add=$this->userimage_model->add(array(
                    "img_name"	=> $img_name,
                    "user_id"	=> $user_id,
                    "is_active"	=> 1,
                    "is_cover"	=> 0
                )

            );
            if ($add) {
                redirect(base_url("users/imageUploadPage/") . $user_id);
            }


        }

    }

    public function deleteImage($id){

        $image = $this->userimage_model->get(array("img_id" => $id));

        $file_name = FCPATH ."uploads\\$image->img_name";

        /* print_r(BASEPATH);C:\xampp\htdocs\coklu_oturum_login\system\

         print_r(APPPATH);C:\xampp\htdocs\coklu_oturum_login\application\

         print_r(FCPATH); C:\xampp\htdocs\coklu_oturum_login\
         die();*/

        if (file_exists($file_name)) {

            unlink($file_name);

            $delete = $this->userimage_model->delete(array("img_id"	=> $id));

            if($delete){

                redirect("users/imageUploadPage/$image->user_id");

            }
        }
        else
        {
            $delete = $this->userimage_model->delete(array("img_id"	=> $id));

            if($delete){

                redirect("users/imageUploadPage/$image->user_id");

            }
        }


    }


    public function isActiveSetterForImage(){

        $id 	  = $this->input->post("id");
        $is_active = ($this->input->post("is_active") == "true") ? 1 : 0;
        echo "isactive : ".$this->input->post("is_active");
        echo "<hr>";
        echo "id : ".$id;

        $update = $this->userimage_model->update(
            array("img_id" => $id),
            array("is_active" => $is_active)
        );

    }

    public function isCoverSetterForImage(){

        $id 	  = $this->input->post("id");
        $is_cover = ($this->input->post("is_cover") == "true") ? 1 : 0;
        echo "isactive : ".$this->input->post("is_cover");
        echo "<hr>";
        echo "id : ".$id;

        $update = $this->userimage_model->update(
            array("img_id" => $id),
            array("is_cover" => $is_cover)
        );

    }



}