<?php
class Ayarlar extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function newPage(){

        $this->load->view("panel/new_user");
    }




    public function editPage($id){

        $viewData = new stdClass();

        $viewData->row = $this->user_model->get(array("id" => $id));

        $this->load->view("panel/edit_user", $viewData);


    }

    public function edit($id){

        $data = array(
            "kullanici_ad" => $this->input->post("title")
        );

        $update = $this->user_model->update(
            array("id"	=> $id),
            $data
        );

        if($update){
            redirect(base_url("ayarlar"));
        }else{
            redirect(base_url("ayarlar/editPage/$id"));
        }
    }







    public function userImageRankUpdate(){

        parse_str($this->input->post("data"), $data);


        print_r($this->input->post("data"));
        die();
        $items = $data["sortId"];

        foreach($items as $rank => $id){

            $this->userimage_model->update(
                array(
                    "img_id"      => $id,
                    "rank !=" => $rank
                ),
                array("rank" => $rank)
            );

        }

    }

    public function isActiveSetterForImage(){

        $id 	  = $this->input->post("id");
        $is_active = ($this->input->post("is_active") == "true") ? 1 : 0;
        echo "isactive : ".$this->input->post("is_active");
        echo "<hr>";
        echo "id : ".$id;

        $update = $this->userimage_model->update(
            array("img_id" => $id),
            array("is_active" => $is_active)
        );

    }

    public function isCoverSetterForImage(){

        $id 	  = $this->input->post("id");
        $is_cover = ($this->input->post("is_cover") == "true") ? 1 : 0;
        echo "isactive : ".$this->input->post("is_cover");
        echo "<hr>";
        echo "id : ".$id;

        $update = $this->userimage_model->update(
            array("img_id" => $id),
            array("is_cover" => $is_cover)
        );

    }
}