<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 18.06.2019
 * Time: 11:58
 */
class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public  function index()
    {
        $this->load->view("panel/dashboard");
    }


    public function ayarlar_page()
    {
        $this->session->set_userdata("baseUrl", base_url());
        //print_r($_SESSION);
        $users_list= $this->user_model->getall();
        $v_data = new stdClass();
        $v_data->rows = $users_list;
        $v_data->admin = true;
        $v_data->num_rows = count($users_list);
        $this->load->view("panel/users",$v_data);
    }

    public function allusers_img()
    {
        $users_imglist= $this->userimage_model->get_all(
            array(),
            "user_id  ASC"
        );
        $v_data = new stdClass();
        $v_data->rows = $users_imglist;
        $v_data->admin = true;
        $v_data->num_rows = count($users_imglist);
        $this->load->view("panel/users_images", $v_data);
    }

    public function adminlogout(){

        $v_data = new stdClass();
        $v_data->admin = true;

        $this->load->view("Front-facesite/login_v",$v_data);
    }
    public function adminlogin(){

        $v_data = new stdClass();
        $v_data->admin = true;

        $this->load->view("Front-facesite/login_v",$v_data);
    }

    public function admingiris(){
        $_SESSION["baseUrl"] = base_url();
        $admin = $this->user_model->get(
            array(
                "email" => $this->input->post("eposta"),
                "password"  => $this->input->post("sifre"),
                "is_administer"  => "1"
            )
        );


        if ($admin) {
            $v_data = new stdClass();
            $v_data->isadmin = true;
            $v_data->admin = $admin;
            $this->load->view("panel/dashboard",$v_data);

        }else{

            $v_data = new stdClass();
            $v_data->noadmin = true;

            $this->load->view("Front-facesite/login_v",$v_data);
        }


    }




    public function editPage($id){

        $viewData = new stdClass();

        $viewData->row = $this->user_model->get(array("id" => $id));

        $this->load->view("panel/edit_user", $viewData);


    }

    public function edit($id){

        $data = array(
            "kullanici_ad" => $this->input->post("title")
        );

        $update = $this->user_model->update(
            array("id"	=> $id),
            $data
        );

        if($update){
            redirect(base_url("ayarlar"));
        }else{
            redirect(base_url("ayarlar/editPage/$id"));
        }
    }



    public function delete($id)
    {
       $deleting= $this->user_model->delete(array('id'=>$id));

        $users_list= $this->session->userdata("loginuser_liste");
        print_r($users_list);
        if (isset($users_list)) {

            foreach ($users_list as $deleteuser) {
                if ($deleteuser->id==$id) {

                    unset($users_list[sha1($deleteuser->email)]);
                    $this->session->set_userdata("loginuser_liste",$users_list);

                }
            }
            redirect(base_url("userlist"));
        }
        else

            redirect(base_url("userlist"));

    }


    public function isAdministerSetter(){

        $id 	  = $this->input->post("id");
        $is_administer = ($this->input->post("isAdminister") == "true") ? 1 : 0;

        $update = $this->user_model->update(
            array("id" => $id),
            array("is_administer" => $is_administer)
        );

    }


    public function imageUploadPage($kullanici_id){

        $this->session->set_userdata("user_id", $kullanici_id);
  //      $this->session->unset_userdata("kullanici_id");
 //       print_r($_SESSION);
//        die();
        $record_rows=$this->userimage_model->get_all(
            array(
                "user_id"	=> $kullanici_id,
            ),
            "rank ASC"
        );
        $_vData = new stdClass();
        $_vData->admin = true;
        $_vData->single = true;
        $_vData->rows = $record_rows;
        $_vData->num_rows = count($record_rows);
        //print_r($_vData->rows);


        $this->load->view("panel/users_images", $_vData);

    }

    public function uploadimage(){

       echo "upload<hr>";
//        die();

        $config['upload_path']          = 'uploads/';
        //$config['allowed_types']        = '*';
        $config['allowed_types']        = 'jpg|gif|png|mp4';
        $config['encrypt_name']			= true;
        $config['max_size']             = 100000;

        /* $config['max_width']            = 1024;
         $config['max_height']           = 768;*/

        $this->load->library('upload', $config);

//		if(!file_exists(FCPATH)){}

        if ( ! $this->upload->do_upload('file'))
        {
            $error = array('error' => $this->upload->display_errors());

            print_r($error);

        }
        else
        {

            // Upload Basarili ise DB ye aktar..
            $data = array('upload_data' => $this->upload->data());
            print_r($data);
            echo "<hr>";
            $img_name = $data["upload_data"]['file_name'];
            print_r($img_name);
 //           die();

            $this->userimage_model->add(array(
                    "img_name"	=> $img_name,
                    "user_id"	=> $this->session->userdata("user_id"),
                    "is_active"	=> 1,
                    "is_cover"	=> 1
                )

            );


        }

    }

    public function deleteImage($id){

        $image = $this->userimage_model->get(array("img_id" => $id));

        $file_name = FCPATH ."uploads\\$image->img_name";

       /* print_r(BASEPATH);C:\xampp\htdocs\coklu_oturum_login\system\

        print_r(APPPATH);C:\xampp\htdocs\coklu_oturum_login\application\

        print_r(FCPATH); C:\xampp\htdocs\coklu_oturum_login\
        die();*/

        if (file_exists($file_name)) {

            unlink($file_name);

            $delete = $this->userimage_model->delete(array("img_id"	=> $id));

            if($delete){

                redirect(base_url("admin/imageUploadPage/$id"));

            }
        }
        else
        {
            $delete = $this->userimage_model->delete(array("img_id"	=> $id));

            if($delete){

                redirect(base_url("allusersimg"));

            }
        }


    }

    public function userImageRankUpdate(){

        parse_str($this->input->post("data"), $data);

        $items = $data["sortId"];

        foreach($items as $rank => $id){

            $this->userimage_model->update(
                array(
                    "img_id"      => $id,
                    "rank !=" => $rank
                ),
                array("rank" => $rank)
            );

        }

    }

    public function isActiveSetterForImage(){

        $id 	  = $this->input->post("id");
        $is_active = ($this->input->post("is_active") == "true") ? 1 : 0;
        echo "isactive : ".$this->input->post("is_active");
        echo "<hr>";
        echo "id : ".$id;

        $update = $this->userimage_model->update(
            array("img_id" => $id),
            array("is_active" => $is_active)
        );

    }

    public function isCoverSetterForImage(){

        $id 	  = $this->input->post("id");
        $is_cover = ($this->input->post("is_cover") == "true") ? 1 : 0;
        echo "isactive : ".$this->input->post("is_cover");
        echo "<hr>";
        echo "id : ".$id;

        $update = $this->userimage_model->update(
            array("img_id" => $id),
            array("is_cover" => $is_cover)
        );

    }
}