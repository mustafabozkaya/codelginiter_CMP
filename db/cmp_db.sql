-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 02 Tem 2019, 17:41:06
-- Sunucu sürümü: 10.1.38-MariaDB
-- PHP Sürümü: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `php_adminsite`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `kullanicilar`
--

CREATE TABLE `kullanicilar` (
  `id` int(11) NOT NULL,
  `is_administer` int(1) NOT NULL,
  `kullanici_ad` varchar(50) COLLATE utf8_turkish_ci NOT NULL,
  `cinsiyet` int(2) NOT NULL DEFAULT '1',
  `email` varchar(150) COLLATE utf8_turkish_ci NOT NULL,
  `password` varchar(300) COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `kullanicilar`
--

INSERT INTO `kullanicilar` (`id`, `is_administer`, `kullanici_ad`, `cinsiyet`, `email`, `password`, `tarih`) VALUES
(5, 1, 'mustafabozkaya', 1, 'mustafaegebozkaya555@gmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2019-06-12 21:00:14'),
(31, 1, 'adnanboz', 1, 'adnan@hotmail.com', '7c4a8d09ca3762af61e59520943dc26494f8941b', '2019-06-26 15:41:10');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `sitebilgileri`
--

CREATE TABLE `sitebilgileri` (
  `sid` int(11) NOT NULL,
  `skey` varchar(60) COLLATE utf8_turkish_ci NOT NULL,
  `svalue` varchar(1000) COLLATE utf8_turkish_ci NOT NULL,
  `tarih` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci;

--
-- Tablo döküm verisi `sitebilgileri`
--

INSERT INTO `sitebilgileri` (`sid`, `skey`, `svalue`, `tarih`) VALUES
(5, 'pagetitle', 'Coffee Break a Blog Category Flat Bootstarp responsive Website Template', '2019-06-07 14:53:57'),
(6, 'admin_hometitle', 'Pooled Admin Panel Category Flat Bootstrap Responsive Web Template', '2019-06-07 14:53:57');

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user_images`
--

CREATE TABLE `user_images` (
  `img_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `img_name` varchar(255) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `is_cover` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Tablo döküm verisi `user_images`
--

INSERT INTO `user_images` (`img_id`, `user_id`, `img_name`, `is_active`, `rank`, `is_cover`) VALUES
(112, 31, '1891e72f076a709acdaec31fa6593faa.jpg', 1, 0, 0),
(113, 31, '3e3b6f57dd346de32628688aa6ff1b56.jpg', 1, 0, 0),
(114, 31, 'bde323efa7e0d3e63db19f54ccad70e9.jpg', 1, 0, 0),
(115, 31, 'cb5933f35a3643d753e0f6cd81aa3f81.jpg', 1, 0, 0),
(116, 31, '6ced26f59f05a6326e41304081a06c88.png', 1, 0, 0);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `kullanicilar`
--
ALTER TABLE `kullanicilar`
  ADD UNIQUE KEY `id` (`id`);

--
-- Tablo için indeksler `sitebilgileri`
--
ALTER TABLE `sitebilgileri`
  ADD PRIMARY KEY (`sid`),
  ADD UNIQUE KEY `skey` (`skey`);

--
-- Tablo için indeksler `user_images`
--
ALTER TABLE `user_images`
  ADD PRIMARY KEY (`img_id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `kullanicilar`
--
ALTER TABLE `kullanicilar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Tablo için AUTO_INCREMENT değeri `sitebilgileri`
--
ALTER TABLE `sitebilgileri`
  MODIFY `sid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Tablo için AUTO_INCREMENT değeri `user_images`
--
ALTER TABLE `user_images`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
